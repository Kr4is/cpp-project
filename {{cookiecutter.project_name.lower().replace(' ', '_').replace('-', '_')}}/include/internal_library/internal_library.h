#ifndef {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}_INTERNAL_LIBRARY_INTERNAL_LIBRARY_H
#define {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}_INTERNAL_LIBRARY_INTERNAL_LIBRARY_H

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

namespace {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}} {
namespace internal_library {

/// \brief Example library class
class InternalLibrary {
 public:
  InternalLibrary();
  ~InternalLibrary();

  /// \brief Example public library function
  /// \return return true
  bool isThisAnInternalLibrary();

 private:
  std::shared_ptr<spdlog::logger> _logger;
  int internal_library_private_ = 0;
};

}  // namespace internal_library
}  // namespace {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}

#endif  // INTERNAL_LIBRARY_INTERNAL_LIBRARY_H
