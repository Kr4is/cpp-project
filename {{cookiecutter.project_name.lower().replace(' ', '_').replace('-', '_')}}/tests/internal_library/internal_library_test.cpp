#include <gtest/gtest.h>
#include <internal_library/internal_library.h>

using namespace {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}::internal_library;

TEST(InternalLibrary, isThisAnInternalLibrary) {
  InternalLibrary internal_library;
  ASSERT_EQ(internal_library.isThisAnInternalLibrary(), true);
}
