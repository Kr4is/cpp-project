# How to contribute

All new functionalities, bug fixes, new documentation, etc. should be submitted via pull/merge requests.

## Development Environment

### Requirements

- Python >= 3.6
- Pip
- The following tools:
  * pre-commit: `pip install pre-commit`
  * cmake-format: `pip install cmakelang`
  * clang-format: `sudo apt install clang-format`
  * clang-tidy: `sudo apt install clang-tidy`

## Branching Model: Git Flow

We are going to follow the Git Flow branch scheme, so we are going to use the following branches:

* The production branch
* The develop branch
* Feature branches
* Release branches
* Hotfix branches

We are also going to use the following naming convention for the different branches:

* Production branch: `master`
* Develop branch: `develop`
* Feature prefix: `feature/<branch-short-description>`
* Hotfix prefix: `hotfix/<branch-short-description>`
* Release prefix: `release/<release-version>`

The overall flow of Gitflow is:

- A develop branch is created from master
- A release branch is created from develop
- Feature branches are created from develop
- When a feature is complete it is merged into the develop branch
- When the release branch is done it is merged into develop and master
- If an issue in master is detected a hotfix branch is created from master
- Once the hotfix is complete it is merged to both develop and master

![Git Flow](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fbemobile.es%2Fblog%2Fwp-content%2Fuploads%2F2016%2F11%2FGitFlow-workflow.png)

## Code Style

We will use the following tools and definitions.

* Code style defintion: [Google Style](https://google.github.io/styleguide/cppguide.html)
* Linters: [clang-tidy](https://clang.llvm.org/extra/clang-tidy/), [cmake-lint](https://cmake-format.readthedocs.io/en/latest/cmake-lint.html)
* Formatters: [clang-format](https://clang.llvm.org/docs/ClangFormat.html), [cmake-format](https://cmake-format.readthedocs.io/en/latest/cmake-format.html)
* Testing: [Google Test + Google Mock](https://google.github.io/googletest/)

We are not expecting anyone to actually read and memorize everything so we are using pre-commit hooks to format and lint the code.
To install it, just run the following command in the root of the project:
```bash
pre-commit install
```

## Code Documentation

As the project documentation is self-generated with doxygen, it is necessary to comment the code so that it is generated correctly.

To add these comments to generate documentation we are going to use the following syntax: `/// ... comment ...`.

There are different keywords to make these comments, the most common are the following:

- `\brief`: Brief description.
- `\param`: Function parameter.
- `\return`: Function return.

Example:

```c++
/// \brief Example function description.
/// \param example an integer example argument.
/// \return The example result.
int example(int example);
```

Further reading:
[Doxygen documentation](https://www.doxygen.nl/manual/docblocks.html)

By default a compilation target is created to generate the documentation. To generate the documentation, just run the following command:
```bash
make doc
```

## Testing

Pull/Merge requests without tests make the baby Jesus cry and, therefore, pull/merge requests
without tests shall not be merged.

Test compilation is disabled by default. To compile the unit tests use the following Cmake flag: `-DBUILD_TESTS=ON`

Cmake example command building unit tests:
```bash
cmake .. -DBUILD_TESTS=ON
```
To compile the test, just run the following command:
```bash
make tests
```

![Jesus doing what Jesus do best](https://i.imgflip.com/4f2h1n.jpg)
