#include <internal_library/internal_library.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include <args.hxx>
#include <iostream>

int main(int argc, char *argv[]) {
  args::ArgumentParser parser("{{cookiecutter.tool_name}}", "version 0.0.0");
  args::HelpFlag help(parser, "help", "Show this help menu.", {'h', "help"});
  args::ValueFlag<std::string> log_level(parser, "log_level",
                                         "Set the execution log level",
                                         {'l', "log-level"}, "info");

  try {
    parser.ParseCLI(argc, argv);
  } catch (args::Help &) {
    std::cout << parser;
    return 0;
  } catch (args::ParseError &e) {
    std::cerr << e.what() << std::endl;
    std::cerr << parser;
    return 1;
  } catch (args::ValidationError &e) {
    std::cerr << e.what() << std::endl;
    std::cerr << parser;
    return 1;
  }

  auto logger = std::make_shared<spdlog::logger>(
      "{{cookiecutter.tool_name}}", std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
  logger->set_level(spdlog::level::from_str(args::get(log_level)));

  logger->info("Starting...");
  logger->info("Started!");

  logger->info("Stopping...");
  logger->info("Stopped!");
  return 0;
}
