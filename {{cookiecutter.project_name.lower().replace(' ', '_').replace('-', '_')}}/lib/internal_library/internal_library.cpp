#include <internal_library/internal_library.h>

using namespace {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}::internal_library;

InternalLibrary::InternalLibrary() {
  std::make_shared<spdlog::logger>(
      "Internalibrary",
      std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
  internal_library_private_ = 1;
}

InternalLibrary::~InternalLibrary() { internal_library_private_ = 2; }

bool InternalLibrary::isThisAnInternalLibrary() { return true; }
