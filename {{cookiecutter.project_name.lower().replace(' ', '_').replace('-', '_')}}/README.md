# {{cookiecutter.project_name}}

{{cookiecutter.project_description}}.

## Requirements

- Conan >= 1.43.2
- Cmake >= 3.20.0

## Build

```bash
mkdir build && cd build
cmake ..
make -j$(nproc)
```

## Usage

```bash
bin/{{cookiecutter.tool_name}} {OPTIONS}

{{cookiecutter.tool_name}}

OPTIONS:

    -h, --help                        Show this help menu.
    -l[log_level],
    --log-level=[log_level]           Set the execution log level

version 0.0.0
```

Example:

```bash
bin/{{cookiecutter.tool_name}} --log-level info
```

## Changelog

To see the change history of the latest versions read the [changelog section](CHANGELOG.md).

## Contributing

Please read the [contribution guidelines](CONTRIBUTING.md) before starting work on a pull/merge request.
